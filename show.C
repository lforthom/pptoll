void show() {
  TH1D *pt1, *pt2, *y1, *y2;
  Double_t _pt1,_pt2,_y1,_y2;
  TCanvas *c1,*c2,*c3,*c4;

  pt1 = new TH1D("pt1", "p_{T}(#mu_{1})", 100, 0., 50.);
  pt2 = new TH1D("pt2", "p_{T}(#mu_{2})", 100, 0., 50.);
  y1 = new TH1D("y1", "y(#mu_{1})", 50, -2.5, 2.5);
  y2 = new TH1D("y2", "y(#mu_{2})", 50, -2.5, 2.5);

  ifstream f("events.dat");
  while (f>>_pt1>>_pt2>>_y1>>_y2) {
    pt1->Fill(_pt1);
    pt2->Fill(_pt2);
    y1->Fill(_y1);
    y2->Fill(_y2);
  }
  c1 = new TCanvas();
  pt1->Draw();
  c2 = new TCanvas();
  pt2->Draw();
  c3 = new TCanvas();
  y1->Draw();
  c4 = new TCanvas();
  y2->Draw();

}
