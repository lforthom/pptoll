        subroutine F2_Fiore(x,Q2,F2)
        implicit real*8 (a-h,o-z)
        real*8 q2

        dimension iswitch(4)
        dimension fit(2,4,5),s0(2),anorm(2),spin(4)

        amp= 0.939d0
        alpha_em = 1.d0/137.d0
        pi = dacos(-1.d0)
        akin = 1.d0 + 4.d0*amp**2*x**2/q2

        prefactor = q2*(1.d0-x)
     >  /(4.d0*pi*alpha_em*akin)

c        write(*,*) 'prefactor=',prefactor

        s = Q2*(1.d0-x)/x + amp**2 

        s0(1) = 1.14d0
        s0(2) = 1.2871d0

        anorm(1) = 0.021d0
        anorm(2) = 0.0207d0

        spin(1) = 3.d0/2.d0
        spin(2) = 5.d0/2.d0
        spin(3) = 3.d0/2.d0
        spin(4) = 1.d0

        iswitch(1) = 1
        iswitch(2) = 1
        iswitch(3) = 1
        iswitch(4) = 1

c       --------------------------

        fit(1,1,1) = -0.8377d0
        fit(1,1,2) =  0.95d0
        fit(1,1,3) =  0.1473d0
        fit(1,1,4) =  1.d0
        fit(1,1,5) =  2.4617d0

        fit(1,2,1) = -0.37d0
        fit(1,2,2) =  0.95d0
        fit(1,2,3) =  0.1471d0
        fit(1,2,4) =  0.5399d0
        fit(1,2,5) =  2.4617d0

        fit(1,3,1) =  0.0038d0
        fit(1,3,2) =  0.85d0
        fit(1,3,3) =  0.1969d0
        fit(1,3,4) =  4.2225d0
        fit(1,3,5) =  1.5722d0

        fit(1,4,1) =  0.5645d0
        fit(1,4,2) =  0.1126d0
        fit(1,4,3) =  1.3086d0
        fit(1,4,4) =  19.2694d0
        fit(1,4,5) =  4.5259d0

c       --------------------------

        fit(2,1,1) = -0.8070d0
        fit(2,1,2) =  0.9632d0
        fit(2,1,3) =  0.1387d0
        fit(2,1,4) =  1.d0
        fit(2,1,5) =  2.6066d0

        fit(2,2,1) = -0.3640d0
        fit(2,2,2) =  0.9531d0
        fit(2,2,3) =  0.1239d0
        fit(2,2,4) =  0.6086d0
        fit(2,2,5) =  2.6066d0

        fit(2,3,1) =  -0.0065d0
        fit(2,3,2) =  0.8355d0
        fit(2,3,3) =  0.2320d0
        fit(2,3,4) =  4.7279d0
        fit(2,3,5) =  1.4828d0

        fit(2,4,1) =  0.5484d0
        fit(2,4,2) =  0.1373d0
        fit(2,4,3) =  1.3139d0
        fit(2,4,4) =  14.7267d0
        fit(2,4,5) =  4.6041d0

c       --------------------------------     

        ifit = 2
        ampli_res = 0.d0
        do ires = 1,3

        alpha_0 = fit(ifit,ires,1) 
        alpha_1 = fit(ifit,ires,2) 
        alpha_2 = fit(ifit,ires,3) 

        s_thr = s0(ifit)

        if (s.gt.s_thr) then

        alpha_Re = alpha_0 + alpha_2*dsqrt(s_thr) + alpha_1*s
        alpha_Im = alpha_2*dsqrt(s-s_thr)

        else

        alpha_Re = alpha_0 + alpha_1*s + 
     >          alpha_2*(dsqrt(s_thr) - dsqrt(s_thr - s))
        
        alpha_Im = 0.d0

        endif


        Q02 = fit(ifit,ires,5) 
        formfactor = 1.d0/(1.d0 + Q2/Q02)**2

        a = fit(ifit,ires,4)
        sp = spin(ires)
        denom = (sp-alpha_Re)**2 + alpha_Im**2

        ampli_imag = a*formfactor**2*alpha_Im/denom


        j = iswitch(ires)

        ampli_res = ampli_res + j* ampli_imag
        enddo

        s_E = fit(ifit,4,3)
        aBG_0 = fit(ifit,4,1)
        aBG_2 = fit(ifit,4,2)

        if (s.gt.s_E) then

        alpha_Re = aBG_0 + aBG_2*dsqrt(s_E)
        alpha_Im = aBG_2*dsqrt(s-s_E)

        else

        alpha_Re = aBG_0 +
     >          aBG_2*(dsqrt(s_E) - dsqrt(s_E - s))
        
        alpha_Im = 0.d0

        endif

        a = fit(ifit,4,4)
        Q02 =  fit(ifit,4,5)

        formfactor = 1.d0/(1.d0 + Q2/Q02)**2

        sp = 1.5*spin(4)
        denom = (sp-alpha_Re)**2 + alpha_Im**2

        j = iswitch(4)
        ampli_bg = a*formfactor**2*alpha_Im/denom
        an = anorm(ifit)
        ampli_tot = an*(ampli_res+j*ampli_bg)


        F2 = prefactor*ampli_tot

        return
        end
